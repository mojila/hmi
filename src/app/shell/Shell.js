import './index.scss'

import React, { useReducer } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import Home from '../routes/home/Home'

// Routes
import routes from '../constants/routes'

// Init Context
import { Context, Reducer, InitialState } from '../context' 

export default () => {
  const [store, dispatch] = useReducer(Reducer, InitialState)

  return (
    <Context.Provider value={{ store, dispatch }}>
      <BrowserRouter>
        <Switch>
          <Route exact path={routes.home} component={Home}/>
        </Switch>
      </BrowserRouter>
    </Context.Provider>
  )
}