import React from "react"

export const InitialState = { 
  user: 'Hello'
}

export const Reducer = (state, action) => {
  switch (action.type) {
    case "change":
      return { user: action.value }
    default:
      return state
  }
}

export const Context = React.createContext()